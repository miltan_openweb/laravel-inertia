<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Users

Route::get('/', [UserController::class, 'index'])->middleware("guest");
  
Route::get('/create', [UserController::class, 'create'])
    ->name('create')->middleware("guest");

Route::post('/store', [UserController::class, 'store'])
    ->name('store')->middleware("guest");

Route::get('/edit/{user}', [UserController::class, 'edit'])
    ->name('edit')->middleware("guest");;

Route::put('update/{user}', [UserController::class, 'update'])
    ->name('update')->middleware("guest");

Route::delete('/delete/{user}', [UserController::class, 'destroy'])
    ->middleware("guest");
