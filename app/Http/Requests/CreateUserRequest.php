<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\User;

class CreateUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:50',
            'last_name' => 'required|max:50',
            'email_address' => 'required|max:50|email',
            'date_of_birth' => 'required|max:50',
            'age' => 'required|max:100|numeric',
            'home_phone' => 'required|max:20|regex:/\(?\d{3}\)?-? *\d{3}-? *-?\d{4}/',
            'mobile_phone' => 'required|max:20|regex:/\(?\d{3}\)?-? *\d{3}-? *-?\d{4}/',
            'street_address' => 'required|max:1000|regex:/\d{1,5}\s\w.\s(\b\w*\b\s){1,2}\w*\./',
            'city' => 'required|max:100',
            'state' => 'required|max:100',
            'zip_code' => 'required|max:10|regex:/^(?:(\d{5})(?:[ \-](\d{4}))?)$/',
            'current_time_at_home_address' => 'required',
        ];
    }

     public function messages()
    {
        
        return [
        ];   
       
    }
}
