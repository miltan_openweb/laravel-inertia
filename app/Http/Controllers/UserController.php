<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use Illuminate\Http\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Validation\Rule;
use Session;
use Inertia\Inertia;
use App\Http\Requests\CreateUserRequest;



class UserController extends Controller
{
    public function index()
    {        
        $data = User::all();
        return Inertia::render('User/Index', ['users' => $data]);
    }

    public function create()
    {
        return Inertia::render('User/Create');
    }

    public function store(CreateUserRequest $request)
    {        
        if(User::create($request->all())){  
            return redirect('/')->with('success', 'User Created.');
        }else{
            return redirect('/')->with('error', 'User not created.');
        }    
    }
    public function edit(User $user)
    {
        return Inertia::render('User/Edit', [
            'user' => [
                'id' => $user->id,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'email_address' => $user->email_address,
                'date_of_birth' => $user->date_of_birth,
                'age' => $user->age,
                'home_phone' => $user->home_phone,
                'mobile_phone' => $user->mobile_phone,
                'street_address' => $user->street_address,
                'city' => $user->city,
                'state' => $user->state,
                'zip_code' => $user->zip_code,
                'current_time_at_home_address' => $user->current_time_at_home_address
            ],
        ]);
    }
    public function update(CreateUserRequest $request)
    {
        
        if ($request->has('id')) {
            if(User::find($request->input('id'))->update($request->all())){
                return redirect('/')->with('success', 'User Updated.');
            }else{
                return redirect('/')->with('success', 'User not updated.');
            }
        }
        
    }

    public function destroy(User $user)
    {   
        if($user->delete()){
            return redirect('/')->with('success', 'User Deleted.');
        }else{
            return redirect('/')->with('error', 'User not deleted.');
        }    
    }


}
